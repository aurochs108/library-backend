package com.dawid.librarybackend.repository;

import com.dawid.librarybackend.entity.Book;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface BookRepository extends JpaRepository<Book, Long> {

    @Override
    List<Book> findAll();

    @Override
    <S extends Book> S save(S s);

    @Override
    void delete(Book book);
}
