package com.dawid.librarybackend.controller;

import com.dawid.librarybackend.entity.Book;
import com.dawid.librarybackend.repository.BookRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.util.List;
import java.util.Optional;

@RestController
public class BookController {

    @Autowired
    private BookRepository bookRepository;

    @GetMapping("/books")
    public List<Book> getAllBooks() {
        return bookRepository.findAll();
    }

    @PostMapping("/books")
    public ResponseEntity<Object> createBook(@RequestBody Book book) {
        Book savedBook = bookRepository.save(book);

        URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}")
                .buildAndExpand(savedBook.getId()).toUri();

        return ResponseEntity.created(location).build();
    }

    @DeleteMapping("/books/{id}")
    public void deleteStudent(@PathVariable long id) {
        bookRepository.deleteById(id);
    }

    @PutMapping("/books/{id}")
    public ResponseEntity<Object> updateStudent(@RequestBody Book book, @PathVariable long id) {

        Optional<Book> bookOptional = bookRepository.findById(id);

        if (!bookOptional.isPresent())
            return ResponseEntity.notFound().build();

        book.setId(id);

        bookRepository.save(book);

        return ResponseEntity.noContent().build();
    }
}
